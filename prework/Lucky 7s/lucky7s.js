function hideResults() {
    document.getElementById("results").style.display = "none";
}
function go() {
  var startingBet= parseInt(document.getElementById("initialBet").value);
  if (startingBet <= 0) {
		alert("Please input an amount greater than 0");
  }
  else {
    var bet= startingBet;
  }
  var dice1 = Math.floor(Math.random() * 6) + 1;
  var dice2 = Math.floor(Math.random() * 6) + 1;
  var roll = dice1 + dice2;
  var money = [];

  while (bet > 0) {
    if(roll != 7) {
            bet -= 1;
        } else {
            bet += 4;
        }
        money.push(bet);
        var dice1 = Math.floor(Math.random() * 6) + 1;
        var dice2 = Math.floor(Math.random() * 6) + 1;
        var roll = dice1 + dice2;
      }

var rollCount = money.length;
var highestAmount = Math.max.apply(Math, money);
var highestPosition = money.indexOf(highestAmount);
var rollCountHighest = rollCount - highestPosition;

function showResults() {
   document.getElementById("results").style.display = "inline";
   document.getElementById("playButton").innerHTML = "Play Again";
   document.getElementById("amountStarted").innerHTML = "$" + startingBet +".00";
   document.getElementById("totalRolls").innerHTML = rollCount;
   document.getElementById("highestAmountWon").innerHTML = "$" + highestAmount + ".00";
   document.getElementById("rollCountHighestWon").innerHTML = rollCountHighest;
 }
   showResults();
}
