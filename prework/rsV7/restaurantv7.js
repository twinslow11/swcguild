function validateForm() {
    var a = document.forms["contactForm"]["fname"].value;
    if (a == null || a == "") {
        alert("Please enter your name");
        return false;
    }
    var b = document.forms["contactForm"]["femail"].value;
    var c = document.forms["contactForm"]["fphone"].value;
    if ((b == null || b == "") && (c == null || c == "")) {
        alert("Please enter your phone number or email");
        return false;
    }
    var d = document.forms["contactForm"]["freason"].value;
    var e = document.forms["contactForm"]["fadditional"].value;
    if ((d == "Other") && (e == null || e == "")) {
        alert("Please enter additional information");
        return false;
    }
    var f = document.forms["contactForm"]["mday"].checked;
    var g = document.forms["contactForm"]["tday"].checked;
    var h = document.forms["contactForm"]["wday"].checked;
    var i = document.forms["contactForm"]["thday"].checked;
    var j = document.forms["contactForm"]["fday"].checked;
    if (f == false && g==false && h==false && i==false && j==false) {
        alert("Please select at least one day");
        return false;
    }
}
